$(document).ready(function(){
  var cssId = Drupal.settings.taxonomy_select.cssId;
  var row_limit = Drupal.settings.taxonomy_select.row_limit;
  var txt_no_selection = Drupal.t("Please choose as many related terms as you see fit from the categories below.");
  var txt_delete_hint = Drupal.t("Click to remove this term");
  function populate_selected_list() {
    $(".taxonomy-select-control .options .item-list li").removeClass("active");
    var init = new Array;
    $(cssId + " option:selected").each(function() {
      
      $(".taxonomy-select-control .options .terms-option-" + this.value).parent().addClass("active");
      init[this.value] = "<a href=\"javascript:void(0);\" class=\"selected-term-option\" title=\"" + txt_delete_hint +"\" id=\"selected-term-option-" + this.value + "\">" + this.innerHTML.substring(1) + "</a> ";
    });
    init = init.join("");
    if(init.length == 0){
      init = txt_no_selection;
    }
    $("#selected-terms-display").html(init);
    $(".taxonomy-select-control a.selected-term-option").click(function(event){
      $(this).remove();
      var togo = this.id.substring(21);
      $(cssId + " option:selected").each(function() {
        if(this.value == togo){
          $(this).removeAttr("selected");
        }
      });
      $(".taxonomy-select-control .options .terms-option-" + togo).parent().removeClass("active");
      if($("#selected-terms-display").html().replace(/^\s+|\s+$/g,"") == ""){
        $("#selected-terms-display").html(txt_no_selection);
      }
    });
  }

  $(".taxonomy-select-control #parent-tabs a.tab").click(function(event){
    $(".taxonomy-select-control #parent-tabs a.tab").removeClass("selected");
    $("#" + this.id).addClass("selected");
    $(".taxonomy-select-control .option-sets .options").addClass("term-options-hidden");
    $(".taxonomy-select-control .option-sets #term-options-" + this.id.substring(9)).removeClass("term-options-hidden");
  });

  $(".taxonomy-select-control div.item-list ul").each(function(){
    var index = 0;
    var col_count = 0;
    $(this).children().each(function (){
      col_count = Math.floor(index/row_limit);
      $(this).addClass('col-' + col_count);
      index++;
    });
    col_width = 100/(col_count + 1);
    for(i = 0; i<= col_count; i++){
      $(this).children("li.col-" + i).wrapAll('<div class="col-' + i + '" style="width: ' + col_width +'%; float: left"></div>');
    }
  });

  $(".taxonomy-select-control a.term-option").click(select_term);

  function select_term(event){
		
    var clicked = $(this).attr('class').substring(25);
    $(cssId + " option").each(function() {
      if(this.value == clicked){
        if($(this).attr("selected")){
          $(this).removeAttr("selected");
        }
        else {
          $(this).attr("selected","selected");
        }
      }
    });
    populate_selected_list();
  }

  $(".taxonomy-select-control #parent-tabs a.tab:first").addClass("selected");
  $(".taxonomy-select-control .option-sets .options").addClass("term-options-hidden");
  $(".taxonomy-select-control .option-sets .options:first").removeClass("term-options-hidden");
  populate_selected_list();
  if($(cssId).hasClass("error")){
    $(".taxonomy-select-control").addClass("error");
  }
  $(cssId + "-wrapper").hide();

});
